function deleteText() {
    if ($(".btn-text").length > 0) {
        $(".btn-text").remove();
    }
};

function hideText() {
    setTimeout(function() {
        $(".btn-text").fadeOut(600);;
    }, 1500);
}

$("#btnClick").click(function() {
    deleteText();
    $("main").toggleClass('drawer')
    hideText();
});


$("#btnDobleClick").dblclick(function() {
    deleteText();
    $(".btn-result").append('<p class="btn-text dblcick-text">Me clickeaste 2 veces <i class="em em-eyes" aria-role="presentation" aria-label="EYES"></i></p>');
    hideText();
});

$("#btnHover").hover(function() {
    deleteText();
    $(".btn-result").append('<p class="btn-text hover-text">Me hiciste hover</p>');
    hideText();
});

$(document).keydown(function(e) {
    deleteText();
    let letra = String.fromCharCode(e.keyCode);
    $(".btn-result").append(`<p class="btn-text keydown-text">Te ví, tocaste la tecla ${letra}</p>`);
    hideText();
});

$("#btnFocus").focus(function() {
    deleteText();
    $(".btn-result").append('<p class="btn-text focus-text">Me hiciste foco <i class="em em-flashlight" aria-role="presentation" aria-label="ELECTRIC TORCH"></i></p>');
    hideText();
});

$("#btnMouseLeave").mouseleave(function() {
    deleteText();
    $(".btn-result").append('<p class="btn-text mouseleave-text">No me dejeees</p>');
    hideText();
});